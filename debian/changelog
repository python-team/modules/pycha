pycha (0.7.0-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:02:05 -0500

pycha (0.7.0-2) unstable; urgency=low

  * Depends on python-setuptools to fix FTBFS. Closes: #725578.

 -- Vincent Bernat <bernat@debian.org>  Mon, 07 Oct 2013 23:13:19 +0200

pycha (0.7.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Vincent Bernat ]
  * New upstream release.
     + Add a patch to fix an error in handling legends in chavier.
  * Switch to dh-python.
  * Switch to python-nose for tests.
  * Ship examples.

 -- Vincent Bernat <bernat@debian.org>  Thu, 15 Aug 2013 18:54:29 +0200

pycha (0.6.0-3) unstable; urgency=low

  * Add missing runtime dependencies. Closes: #645265.
  * Bump Standards-Version to 3.9.2.

 -- Vincent Bernat <bernat@debian.org>  Fri, 14 Oct 2011 07:51:19 +0200

pycha (0.6.0-2) unstable; urgency=low

  * Team upload.
  * Rebuild to add Python 2.7 support

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 08 May 2011 16:45:36 +0200

pycha (0.6.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.1.
  * Switch to dh_python2.

 -- Vincent Bernat <bernat@debian.org>  Tue, 22 Feb 2011 20:41:28 +0100

pycha (0.5.3-1) unstable; urgency=low

  * New upstream release

 -- Vincent Bernat <bernat@debian.org>  Fri, 23 Apr 2010 23:21:16 +0200

pycha (0.5.2-2) unstable; urgency=low

  [ Luca Falavigna ]
  * debian/runtests.py:
    - Fix FTBFS with python2.6 as default Python version. Closes: #571503.

  [ Vincent Bernat ]
  * Adjust Build-Depends.
  * Bump Standards-Version to 3.8.4.
  * Switch to 3.0 (quilt) format.
  * Fix Vcs-Svn field.
  * Adapt unittests launcher to support an eventual future change in CDBS.

 -- Vincent Bernat <bernat@debian.org>  Tue, 02 Mar 2010 08:38:18 +0100

pycha (0.5.2-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.3.
  * Update Homepage to reflect the new home of pycha.

 -- Vincent Bernat <bernat@debian.org>  Sun, 27 Sep 2009 15:14:27 +0200

pycha (0.5.0-1) unstable; urgency=low

  * New upstream version
  * Add back unittests with a piece of code stolen from python-axiom.

 -- Vincent Bernat <bernat@debian.org>  Sun, 26 Apr 2009 09:58:05 +0200

pycha (0.4.2-2) unstable; urgency=low

  * Recompile with python-support from unstable. Closes: #520224.
  * Bump Standards-Version to 3.8.1. No changes required.

 -- Vincent Bernat <bernat@debian.org>  Wed, 18 Mar 2009 20:27:10 +0100

pycha (0.4.2-1) unstable; urgency=low

  * New upstream release. Closes: #506290
  * Upload to unstable.
  * Don't run unittests at all. Closes: #516901

 -- Vincent Bernat <bernat@debian.org>  Sat, 14 Mar 2009 15:51:31 +0100

pycha (0.4.1-1) experimental; urgency=low

  [ Vincent Bernat ]
  * New upstream version
  * Update Standards-Version to 3.8.0 (no changes required)
  * Add a manual page for the new program chavier

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Vincent Bernat <bernat@debian.org>  Wed, 19 Nov 2008 20:59:12 +0100

pycha (0.3.0-1) unstable; urgency=low

  * New upstream release
  * Update description to include scatter charts

 -- Vincent Bernat <bernat@luffy.cx>  Thu, 27 Mar 2008 19:29:53 +0100

pycha (0.2.0-1) unstable; urgency=low

  * Initial release (Closes: #458903)

 -- Vincent Bernat <bernat@luffy.cx>  Thu, 03 Jan 2008 15:49:45 +0100
